Getting started with MadhavOS
====================

![MadhavOS](https://telegra.ph/file/ba50769df8b3f48ba87cf.png?raw=true)

Initialize Local Repository
-------------
```bash
  repo init --depth=1 -u https://gitlab.com/Project-Madhav/android_manifest -b 12
```

Syncing Repository
-------------
```bash
  repo sync -c --no-clone-bundle --no-tags --optimized-fetch --prune --force-sync -j$(nproc --all)
```
Note: We Still use `nad` identifier, this may change in future. For now, device adaptation needs to be done viz. `nad_<devicename>`

Lunch Command
-------------
```bash
  . build/envsetup.sh
  lunch madhav_<device_codename>-buildtype
  make madhav
```
